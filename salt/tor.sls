{% set tor_pk = salt['pillar.get']('tor_pk', None) %}

tor-pack:
  pkg.installed:
    - names:
      - tor

tor:
  service:
    - running
    - reload: True
    - require:
      - pkg: tor

/etc/tor/torrc:
  file.managed:
    - source: salt://conf/torrc

{% if tor_pk %}
/var/lib/tor/hidden_service/private_key:
  file.managed:
    - user: root
    - group: root
  file.append:
    - text: {{ tor_pk }}
{% endif %}

reload_tor:
  cmd.run:
    - name: service tor restart
