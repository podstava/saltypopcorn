venv-packages:
  pkg.installed:
    - names:
      - build-essential
      - python-dev
      - python-virtualenv
      - python-pip


pip-upgrade:
  pip.installed:
    - upgrade: True
    - mirrors: http://a.pypi.python.org
    - require:
      - pkg: python-pip

venv:
  virtualenv.managed:
    - name: /var/www/venv
    - system_site_packages: False
    - python: /usr/bin/python
    - require:
      - pkg: build-essential
      - pkg: python-dev
      - pkg: python-virtualenv

