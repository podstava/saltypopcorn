{% set db_user = salt['pillar.get']('dbuser') %}
{% set db_pass = salt['pillar.get']('dbpass') %}
{% set db_host = salt['pillar.get']('dbhost') %}
{% set db_name = salt['pillar.get']('dbname') %}

mysql-pack:
  pkg.installed:
    - names:
      - mysql-server

mysql:
  service:
    - running
  require:
    - pkg.installed: mysql-server
  watch:
    - pkg.installed: mysql-server

mysql-database-managed:
  mysql_database.present:
    - name: {{ db_name }}

mysql-user-managed:
  mysql_user.present:
    - name: {{ db_user }}
    - password: {{ db_pass }}
    - host: {{ db_host }}

mysql-user-grants-managed:
  mysql_grants.present:
    - grant: all
    - host: {{ db_host }}
    - user: {{ db_user }}
    - database: {{ db_name }}.*

