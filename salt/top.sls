base:
  '*':
    - locale
    - user
    - mysql
    - tor
    - nginx
    - uwsgi
    - venv
    - backend
