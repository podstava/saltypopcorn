# Backend setting

{% set app_path = "/var/www/app" %}
{% set static_path = "/var/www/static" %}
{% set media_path = "/var/www/media" %}
{% set venv_path = "/var/www/venv" %}

{% set app_fqdn = salt['pillar.get']('app_fqdn') %}
{% set app_settings = salt['pillar.get']('app_settings') %}
{% set app_name = salt['pillar.get']('app_name') %}
{% set db_host = salt['pillar.get']('dbhost') %}
{% set db_name = salt['pillar.get']('dbname') %}
{% set db_user = salt['pillar.get']('dbuser') %}
{% set db_pass = salt['pillar.get']('dbpass') %}
{% set db_port = salt['pillar.get']('dbport') %}


# Nginx setup

{% if salt['file.directory_exists']('/var/lib/tor/hidden_service/') %}
/etc/nginx/sites-available/backend.conf:
  file.managed:
    - source: salt://conf/nginx-backend.conf
    - template: jinja
    - watch_in:
      - service: nginx
    - defaults:
      server_fqdn: {{ salt['cmd.run']('cat /var/lib/tor/hidden_service/hostname') }}
      static_path: {{ static_path }}
      app_fqdn: {{ app_fqdn }} 
{% else %}
/etc/nginx/sites-available/backend.conf:
  file.managed:
    - source: salt://conf/nginx-backend.conf
    - template: jinja
    - watch_in:
      - service: nginx
    - defaults:
      server_fqdn: 'test.server'
      static_path: {{ static_path }}
{% endif %}


/etc/nginx/sites-enabled/backend.conf:
  file.symlink:
    - target: /etc/nginx/sites-available/backend.conf
    - watch_in:
      - service: nginx

/var/log/nginx/backend:
  file.directory:
    - user: root
    - group: root
    - mode: 755
    - makedirs: True
    - watch_in:
      - service: nginx

static-dir:
  file.directory:
    - name: {{ static_path }}
    - user: www-data
    - group: www-data
    - mode: 755
    - makedirs: True
    - watch_in:
      - service: uwsgi

# UWSGI setup

/etc/uwsgi/apps-available/backend.ini:
  file.managed:
    - source: salt://conf/uwsgi-template.ini
    - template: jinja
    - watch_in:
      - service: uwsgi
    - defaults:
      server_fqdn: {{ app_fqdn }}
      venv_root: {{ venv_path }}
      app_path: {{ app_path }}
      django_settings: {{ app_settings }}

/etc/uwsgi/apps-enabled/backend.ini:
  file.symlink:
    - target: /etc/uwsgi/apps-available/backend.ini
    - watch_in:
      - service: uwsgi

/var/log/uwsgi/backend:
  file.directory:
    - user: root
    - group: root
    - mode: 755
    - makedirs: True
    - watch_in:
      - service: uwsgi

# Sources 

backend-server-sources:
  file.recurse:
    - source: salt://git-backend/stub
    - name: {{ app_path }}
    - user: www-data
    - group: www-data
    - file_mode: '0644'
    - dir_mode: '755'
    - clean: True
    - watch_in:
      - service: uwsgi

pip:
  pip.installed:
    - bin_env: {{ venv_path }}
    - requirements: {{ app_path }}/requirements.txt
    - require:
      - virtualenv: venv
      - file: backend-server-sources
    - watch_in:
      - service: uwsgi

local_settings.py:
  file.managed:
    - name: {{ app_path }}/{{ app_name }}/local_settings.py
    - source: salt://conf/local_settings.py
    - template: jinja
    - require:
      - file: backend-server-sources
    - watch_in:
      - service: uwsgi
    - defaults:
      static_path: {{ static_path }}
      media_path: {{ media_path }}
      db_host: {{ db_host }}
      db_port: {{ db_port }}
      db_user: {{ db_user }}
      db_pass: {{ db_pass }}
      db_name: {{ db_name }}

# migration and static
# Need more info about project to do this

be_migrate:
  module.run:
    - name: django.command
    - command: migrate
    - settings_module: {{ app_settings }}
    - bin_env: {{ venv_path }}
    - pythonpath: {{ app_path }}
    - require:
      - file: local_settings.py

#be_collectstatic:
#  module.run:
#    - name: django.command
#    - command: collectstatic
#    - args:
#      - noinput
#    - settings_module: {{ app_settings }}
#    - bin_env: {{ venv_path }}
#    - pythonpath: {{ app_path }}
#    - require:
#      - file: local_settings.py
#      - file: static-dir

