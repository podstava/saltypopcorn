{% set username = salt['pillar.get']('user').get('username') %}
{% set key = salt['pillar.get']('user').get('pkey') %}
{% set root_user = salt['pillar.get']('root_user') %}

{{ username }}:
  user.present:
    - shell: /bin/bash
    - home: /home/{{ username }}
    - groups:
      - sudo

/home/{{ username }}/.ssh:
  file.directory:
    - user: {{ username }}
    - group: {{ username }}
    - mode: 700

/home/{{ username }}/.ssh/authorized_keys:
  file:
    - managed
    - user: {{ username }}
    - group: {{ username }}
    - source: /home/{{ root_user }}/.ssh/authorized_keys
    - skip_verify: True
    - mode: 600


sudoers:
  file.managed:
    - replace: True
    - name: /etc/sudoers.d/{{ username }}
    - contents: |
        ########################################################################
        # File managed by Salt (users-formula).
        # Your changes will be overwritten.
        ########################################################################
        {{ username }} ALL=(ALL) NOPASSWD:ALL

