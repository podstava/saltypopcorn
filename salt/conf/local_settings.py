DATABASES = {
        'default': {
                    'ENGINE': 'django.db.backends.mysql',
                    'NAME': '{{ db_name }}',
                    'USER': '{{ db_user }}',
                    'PASSWORD': '{{ db_pass }}',
                    'HOST': 'localhost',
                    'PORT': '3306',
                }
}

ALLOWED_HOSTS = ['*']

STATIC_ROOT = '{{ static_path }}'

MEDIA_ROOT = '{{ media_path }}'

