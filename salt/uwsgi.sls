uwsgi-packages:
  pkg.installed:
    - names:
      - uwsgi
      - uwsgi-plugin-python
      - uwsgi-plugin-python3
      - python-dev
      - python-virtualenv

uwsgi:
   service:
     - running
     - reload: True
     - require:
       - pkg: uwsgi

